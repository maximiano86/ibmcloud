resource "ibm_resource_instance" "cos_instance" {
  name              = "cos-instance-dou"
  resource_group_id = "Default"
  service           = "cloud-object-storage"
  plan              = "standard"
  location          = "global"
}